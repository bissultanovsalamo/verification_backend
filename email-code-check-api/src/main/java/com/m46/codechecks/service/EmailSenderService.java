package com.m46.codechecks.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final JavaMailSender javaMailSender;

    public void sendVerificationEmail(String email, String customerName, String code) {
        sendEmail(email, customerName, code);
    }

    public void sendEmail(String email, String customerName, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("bissultanovsalamo@gmail.com");
        message.setTo(email);
        message.setSubject("Code verification");
        message.setText("Hi, "+customerName+" your code is -  "+code);
        javaMailSender.send(message);
    }

}
