package com.m46.codechecks.controller;

import com.m46.codechecks.model.EmailVerification;
import com.m46.codechecks.model.VerificationRequest;
import com.m46.codechecks.service.EmailVerificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/email")
public class EmailVerificationController {

    private final EmailVerificationService verificationService;

    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.CREATED)
    public void requestEmailVerification(@RequestBody VerificationRequest verificationRequest) {
        log.info("New verification for {} {}", verificationRequest.getName(), verificationRequest.getEmail());
        verificationService.requestEmailVerification(verificationRequest.getEmail(), verificationRequest.getName());
    }

    @PostMapping(path = "/verify")
    @ResponseStatus(HttpStatus.OK)
    public void verify(@RequestBody EmailVerification verification) {
        log.info("Verify {} with code '{}'", verification.getEmail(), verification.getVerificationCode());
        verificationService.verify(verification.getEmail(), verification.getVerificationCode());
    }
}
