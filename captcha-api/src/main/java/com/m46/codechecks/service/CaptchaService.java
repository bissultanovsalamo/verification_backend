package com.m46.codechecks.service;

import com.m46.codechecks.model.QuestionEntity;
import com.m46.codechecks.model.dto.AnswerDto;
import com.m46.codechecks.model.dto.QuestionDto;
import com.m46.codechecks.repository.CaptchaRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CaptchaService {

    private final CaptchaRepository captchaRepository;
    private final ModelMapper modelMapper;

    public QuestionDto getQuestion(){
        long randomQuestion = (long) (1+ (Math.random() * 5));
        Optional<QuestionEntity> question = captchaRepository.findById(randomQuestion);
        return modelMapper.map(question.get(), QuestionDto.class);
    }

    public Object postAnswer(AnswerDto answer){
        Optional<QuestionEntity> questionEntity = captchaRepository.findById(answer.getQuestionId());
        if(Objects.equals(answer.getAnswer(),questionEntity.get().getAnswer())){
            return answer;
        }
        return new Object();
    }

}
