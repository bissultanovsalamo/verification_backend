package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class QuestionDto {
    private Long questionId;
    private String question;
}
