package com.m46.codechecks.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "person",schema = "public")
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String name;
    private String surname;
    private String patronymic;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id", updatable = false, insertable = false)
    private List<ProductEntity> products;
}
