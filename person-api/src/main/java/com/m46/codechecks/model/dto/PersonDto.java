package com.m46.codechecks.model.dto;

import lombok.Data;

import java.util.List;


@Data
public class PersonDto {
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    private String email;
    private List<ProductDto> products;
}
