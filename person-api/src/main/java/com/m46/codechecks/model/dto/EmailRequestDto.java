package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class EmailRequestDto {
    private String email;
    private String verificationCode;
}
