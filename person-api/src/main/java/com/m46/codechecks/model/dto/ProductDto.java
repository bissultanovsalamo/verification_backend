package com.m46.codechecks.model.dto;

import lombok.Data;

@Data
public class ProductDto {
    private Long id;
    private String name;
    private String status;
    private Long personId;
}
