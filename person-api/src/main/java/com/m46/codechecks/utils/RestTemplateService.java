package com.m46.codechecks.utils;

import com.m46.codechecks.model.dto.CaptchaDto;
import com.m46.codechecks.model.dto.EmailRequestDto;
import com.m46.codechecks.model.entity.PersonEntity;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class RestTemplateService {

    public void sendEmailCodeTemplate(PersonEntity personEntity){

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        params.put("email", personEntity.getEmail());
        params.put("name", personEntity.getName()+" "+personEntity.getSurname()+" "+personEntity.getPatronymic());

        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://167.172.173.94:8081/email/send";

        ResponseEntity<?> response = new RestTemplate().postForEntity(url, request, String.class);
    }

    public void verifyEmailCode(EmailRequestDto emailRequestDto) {

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        params.put("email", emailRequestDto.getEmail());
        params.put("verificationCode", emailRequestDto.getVerificationCode());

        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://167.172.173.94:8081/email/verify";

        ResponseEntity<?> response = new RestTemplate().postForEntity(url,request, Object.class);

    }

    public Object getCaptchaTemplate(){

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
//        params.put("person_id",personEntity.getId());

        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://167.172.173.94:8082/captcha/get";

        ResponseEntity<Object> response = new RestTemplate().getForEntity(url, Object.class);

        return response.getBody();
    }

    public CaptchaDto postCaptchaTemplate(CaptchaDto captchaQuestion){

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();

        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map params = new HashMap();
        params.put("questionId",captchaQuestion.getQuestionId());
        params.put("answer",captchaQuestion.getAnswer());

        HttpEntity<?> request = new HttpEntity<>(params, headers);
        String url = "http://167.172.173.94:8082/captcha/post";

        ResponseEntity<Object> response = new RestTemplate().postForEntity(url,request, Object.class);
        return captchaQuestion;
    }


}
