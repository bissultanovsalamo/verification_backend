package com.m46.codechecks.repository;

import com.m46.codechecks.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity,Long> {

    List<ProductEntity> getProductEntitiesByPersonId(Long personId);

}
