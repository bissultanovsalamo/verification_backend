package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.ProductDto;
import com.m46.codechecks.model.entity.ProductEntity;
import com.m46.codechecks.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;

    public void addProducts(List<ProductDto> productsList,Long personId){
        productsList.forEach(item -> item.setPersonId(personId));

        List<ProductEntity> productEntities = productsList.stream()
                .peek(item -> item.setStatus("NEW"))
                .map(item -> modelMapper.map(item,ProductEntity.class))
                .collect(Collectors.toList());

        productRepository.saveAll(productEntities);
    }


}
