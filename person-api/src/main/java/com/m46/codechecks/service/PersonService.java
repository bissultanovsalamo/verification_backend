package com.m46.codechecks.service;

import com.m46.codechecks.model.dto.CaptchaDto;
import com.m46.codechecks.model.dto.EmailRequestDto;
import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.model.dto.ProductDto;
import com.m46.codechecks.model.entity.PersonEntity;
import com.m46.codechecks.model.entity.ProductEntity;
import com.m46.codechecks.repository.PersonRepository;
import com.m46.codechecks.repository.ProductRepository;
import com.m46.codechecks.utils.RestTemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PersonService {

    private final ProductService productService;
    private final PersonRepository personRepository;
    private final ProductRepository productRepository;

    private final ModelMapper modelMapper;
    private final RestTemplateService restTemplate;


    public PersonDto register(PersonDto personDto) {
        PersonEntity personEntity = personRepository.save(modelMapper.map(personDto, PersonEntity.class));
        productService.addProducts(personDto.getProducts(),personEntity.getId());
        restTemplate.sendEmailCodeTemplate(personEntity);
        System.out.println();
        return modelMapper.map(personEntity, PersonDto.class);
    }

    public Object verifyEmailCode(EmailRequestDto emailRequestDto) {
        try {
            restTemplate.verifyEmailCode(emailRequestDto);
            return restTemplate.getCaptchaTemplate();
        }catch (RuntimeException e){
            return new Object();
        }
    }

    public Object verifyCaptcha(CaptchaDto question){
        try{
            restTemplate.postCaptchaTemplate(question);
            return updateProductsStatus(question);
        }catch (RuntimeException e){
            return new Object();
        }
    }

    public List<ProductDto> updateProductsStatus(CaptchaDto question){
        List<ProductEntity> products = productRepository
                .getProductEntitiesByPersonId(question.getPersonId());
        products.forEach(item -> item.setStatus("DONE"));
        productRepository.saveAll(products);
        List<ProductDto> dtos = products.stream()
                .map(item -> modelMapper.map(item,ProductDto.class))
                .collect(Collectors.toList());
        return dtos;
    }
}
