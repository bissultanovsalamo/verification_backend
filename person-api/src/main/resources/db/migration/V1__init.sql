create table if not exists person
(id serial primary key,
 email varchar(250) not null,
 name varchar(250) not null,
 surname varchar(250)not null ,
 patronymic varchar(250)
);

create table products(
     id SERIAL primary key ,
     name varchar(250) not null ,
     status varchar(250),
     person_id INTEGER references person(id)
);

create table captcha
(id serial primary key ,question varchar(250) not null ,answer varchar(250) not null );
insert into captcha (question,answer) values ('сколько будет 2+3?','5');
insert into captcha (question,answer) values ('имя первого человека?','адам');
insert into captcha (question,answer) values ('сколько бит в 1 байте?','8');
insert into captcha (question,answer) values ('сколько будет 3-2?','1');
insert into captcha (question,answer) values ('лучшие ноуты - модель?','асус');

CREATE TABLE email_verifications (
    verification_id SERIAL PRIMARY KEY,
    email CHARACTER VARYING(128) NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    verification_code CHARACTER VARYING(16) NOT NULL,
    verification_status CHARACTER VARYING(16),
    message CHARACTER VARYING(256)
);




